import React, { Component } from "react";
import NavBar from "./NavBar";
import DogList from "./DogList";
import { Route , Switch } from 'react-router-dom';
import DogInfo from './DogInfo';
import "./App.css";

import bobi from "./images/bobi.jpg";
import laci from "./images/laci.jpg";
import dino from "./images/dino.jpg";

class App extends Component {

  static defaultProps = {
    dogs: [
      {
        name: "Bobi",
        src: bobi,
        age: 5,
        facts: [
          "Bobi likes eat everything",
          "Bobi is a nice guy",
          "Bobi run fast",
          "Bobi is a good figther"
        ]
      },
      {
        name: "Laci",
        src: laci,
        age: 10,
        facts: [
          "Lacy is a real lady",
          "Lacy likes hunt bugs",
          "Lacy is mother of 2 puppies"
        ]
      },
      {
        name: "Dino",
        src: dino,
        age: 2,
        facts: [
          "Dino has a tough childhood",
          "Dino is the smaller dog is the app"
        ]
      },
      {
        name: "Placido",
        src: dino,
        age: 6,
        facts: ["Placido can swim", "Placido likes water a lot"]
      }
    ]
  };

  render() {

   

    return (
      <div className="App">
        <NavBar dogs={this.props.dogs}/>
        <Switch>
          <Route exact path='/dogs/:name' render={(routeProps) => <DogInfo dogs={this.props.dogs} {...routeProps}/>}/>
          <Route exact path='/dogs' render={() => <DogList dogs={this.props.dogs}/>}/>
        </Switch>
        
      </div>
    );
  }
}

export default App;
