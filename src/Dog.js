import React , { Component } from 'react';
import './Dog.css';

class Dog extends Component {



    render(){
        return(
            <div className='Dog'>
                <img alt={this.props.name} className='DogImage' src={this.props.src} />
                <h4 className="DogName">{this.props.name}</h4>
            </div>
        );
    }
}

export default Dog;