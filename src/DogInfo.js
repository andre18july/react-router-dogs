import React, { Component } from "react";
import './DogInfo.css';

class Dog extends Component {

  render() {
    console.log(this.props.match.params.name);
    let obj = this.props.dogs.find(obj => {
      console.log(obj.name.toUpperCase());
      return (obj.name.toUpperCase() === this.props.match.params.name.toUpperCase());
    });
    console.log(obj);
    return (
      <div className="card DogInfo" style={{width: '18rem'}}>
        <img className="card-img-top" src={obj.src} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{obj.name}</h5>
          <p className="card-text">
            Age: {obj.age}
          </p>
        </div>
        <ul className="list-group list-group-flush">
            {obj.facts.map(fact => <li className="list-group-item">{fact}</li>)}
        </ul>
        <div className="card-body">
          <a href="#" className="card-link">
            Card link
          </a>
          <a href="#" className="card-link">
            Another link
          </a>
        </div>
      </div>
    );
  }
}

export default Dog;
