import React, { Component } from 'react';
import Dog from './Dog';
import './DogList.css';



class DogList extends Component {


    render(){

        //console.log(this.props.dogs);
        const dogs = this.props.dogs.map(dog => <Dog key={dog.name} name={dog.name} src={dog.src} />) 

        return(
            <div>
                <h1 className='display-1'>Dog List</h1>
                <div className="DogList">
                    {dogs}
                </div>
            </div>
        )
    }
}

export default DogList;